import react from "react"
import Button from "./Button"
import FormControl from "./FormControl"

function UserForm() {
  return (
    <>
    <h2>Registration</h2>
    <form action="">
      <FormControl labelText="Enter your Email" inputId="email" type="email"/>
      <FormControl labelText="Text" inputId="text" type="text"/>
      <FormControl labelText="Password" inputId="password" type="password"/>
      <FormControl labelText="Confirm Password" inputId="confirmPassword" type="password"/>
      <FormControl labelText="Enter your comment" inputId="textareaId" type="textarea"/>
      <Button/>
    </form>
    <h2>Edit profile</h2>
    <form action="">
      <FormControl value="test@text.com" labelText="Enter your Email" inputId="email" type="email"/>
      <FormControl value="" labelText="Text" inputId="text" type="text"/>
      <FormControl value="" labelText="Enter your comment" inputId="textareaId" type="textarea"/>
      <Button/>
    </form>
    </>
    
  )
}

export default UserForm